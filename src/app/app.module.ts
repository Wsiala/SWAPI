import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FilmsComponent } from './Components/films/films.component';
import { PeoplesComponent } from './Components/peoples/peoples.component';
import { StarshipsComponent } from './Components/starships/starships.component';
import { PlanetsComponent } from './Components/planets/planets.component';
import { SpeciesComponent } from './Components/species/species.component';
import { VehiculesComponent } from './Components/vehicules/vehicules.component';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { SearchBarComponent } from './Components/search-bar/search-bar.component';
import { HttpErrorInterceptor } from './http-error.interceptor';


@NgModule({
  declarations: [
    AppComponent,
    FilmsComponent,
    PeoplesComponent,
    StarshipsComponent,
    PlanetsComponent,
    SpeciesComponent,
    VehiculesComponent,
    SearchBarComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: HttpErrorInterceptor,
      multi: true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
