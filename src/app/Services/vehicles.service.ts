import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, of } from 'rxjs';
import { catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class VehiclesService {

  private vehicleSelected = new BehaviorSubject({});
  vehicleSelectedObj = this.vehicleSelected.asObservable();
  constructor(
    private http: HttpClient,
    ) { }

  getVehiclesList() {
    return this.http.get('http://swapi.dev/api/vehicles/').toPromise();
  }

  getVehicleById(url):Observable<Object> {
    return this.http.get(url).pipe(
      catchError(err => of([]))
    );
  }

  setVehicleSelected(vehicle) {
    this.vehicleSelected.next(vehicle);
  }
  getVehicleSelected(): Observable<any> {
    return this.vehicleSelectedObj;
  }
}
