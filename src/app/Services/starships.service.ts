import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, BehaviorSubject, throwError, of } from 'rxjs';
import { catchError } from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})
export class StarshipsService {

  private starshipSelected = new BehaviorSubject({});
  starshipSelectedObj = this.starshipSelected.asObservable();

  constructor(
    private http: HttpClient,
    )
    { }

  getStarshipsList() {
    return this.http.get('http://swapi.dev/api/starships/').toPromise();
  }

  getStarshipById(url):Observable<Object> {
    return this.http.get(url).pipe(
      catchError(err => of([]))
    );
  }

  setStarshipSelected(starship) {
    this.starshipSelected.next(starship);
  }
  getStarshipSelected(): Observable<any> {
    return this.starshipSelectedObj;
  }
}
