import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, BehaviorSubject, of } from 'rxjs';
import { catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class PlanetsService {

  private planetSelected = new BehaviorSubject({});
  planetSelectedObj = this.planetSelected.asObservable();

  constructor(
    private http: HttpClient,
    ) { }

  getPlanetsList() {
    return this.http.get('http://swapi.dev/api/planets/').toPromise();
  }

  getPlanetById(url):Observable<Object> {
    return this.http.get(url).pipe(
      catchError(err => of([]))
    );
  }

  setPlanetSelected(planet) {
    this.planetSelected.next(planet);
  }
  getPlanetSelected(): Observable<any> {
    return this.planetSelectedObj;
  }
}
