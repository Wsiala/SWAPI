import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, BehaviorSubject, of } from 'rxjs';
import { catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class FilmsService {
  filmsList;
  private movieSelected = new BehaviorSubject({});
  movieSelectedObj = this.movieSelected.asObservable();

  constructor(
    private http: HttpClient
  ) { }

  getFilmsList() {
    return this.http.get('http://swapi.dev/api/films/').toPromise();
  }

  getMoviesById(url):Observable<Object> {
    return this.http.get(url).pipe(
      catchError(err => of([]))
    );
  }

  setMovieSelected(movie) {
    this.movieSelected.next(movie);
  }
  getMovieSelected(): Observable<any> {
    return this.movieSelectedObj;
  }
}
