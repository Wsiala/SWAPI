import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, BehaviorSubject, of } from 'rxjs';
import { catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class SpeciesService {

  private speciesSelected = new BehaviorSubject({});
  speciesSelectedObj = this.speciesSelected.asObservable();

  constructor(
    private http: HttpClient,
    ) { }

    getSpeciesList() {
      return this.http.get('http://swapi.dev/api/species/').toPromise();
    }

    getSpeciesById(url):Observable<Object> {
      return this.http.get(url).pipe(
        catchError(err => of([]))
      );
    }

    setSpeciesSelected(planet) {
      this.speciesSelected.next(planet);
    }
    getSpeciesSelected(): Observable<any> {
      return this.speciesSelectedObj;
    }
}
