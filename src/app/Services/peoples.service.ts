import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, BehaviorSubject, of } from 'rxjs';
import { catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class PeoplesService {
  private peopleSelected = new BehaviorSubject({});
  peopleSelectedObj = this.peopleSelected.asObservable();

  constructor(
    private http: HttpClient,
  ) { }

  getPeoplesList() {
    return this.http.get('http://swapi.dev/api/people').toPromise();
  }

  getPeoplesById(url):Observable<Object> {
    return this.http.get(url).pipe(
      catchError(err => of([]))
    );
  }

  setPeopleSelected(people) {
    this.peopleSelected.next(people);
  }
  getPeopleSelected(): Observable<any> {
    return this.peopleSelectedObj;
  }
}
