import { NgModule } from '@angular/core';
import { Routes, RouterModule, ActivatedRoute, ParamMap } from '@angular/router';
import { VehiculesComponent } from './Components/vehicules/vehicules.component';
import { FilmsComponent } from './Components/films/films.component';
import { PeoplesComponent } from './Components/peoples/peoples.component';
import { PlanetsComponent } from './Components/planets/planets.component';
import { SpeciesComponent } from './Components/species/species.component';
import { StarshipsComponent } from './Components/starships/starships.component';
import { SearchBarComponent } from './Components/search-bar/search-bar.component';


const routes: Routes = [
  {
    path: "vehicles",
    component: VehiculesComponent,
  },
  {
    path: "films",
    component: FilmsComponent,
  },
  {
    path: "people",
    component: PeoplesComponent,
  },
  {
    path: "planets",
    component: PlanetsComponent,
  },
  {
    path: "species",
    component: SpeciesComponent,
  },
  {
    path: "starships",
    component: StarshipsComponent
  },
  {
    path: "search",
    component: SearchBarComponent,
  }

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
