import { Component, OnInit, OnDestroy } from '@angular/core';
import { PeoplesService } from 'src/app/Services/peoples.service';
import { FilmsService } from 'src/app/Services/films.service';
import { StarshipsService } from 'src/app/Services/starships.service';
import { PlanetsService } from 'src/app/Services/planets.service';
import { SpeciesService } from 'src/app/Services/species.service';
import { VehiclesService } from 'src/app/Services/vehicles.service';

@Component({
  selector: 'app-peoples',
  templateUrl: './peoples.component.html',
  styleUrls: ['./peoples.component.css']
})
export class PeoplesComponent implements OnInit, OnDestroy {
  peoplesList;
  newPeoplesList;
  nextPeoplesList;
  people_selected;
  peopleMovies;
  peopleStarships;
  counter = 10;
  personeName;
  homeworld;
  peopleSpecies;
  pilotsVehicles;
  urlIndex;
  peopleListObs;
  finishedList = true;


  constructor(
    private peoplesService: PeoplesService,
    private filmsService: FilmsService,
    private starshipsService: StarshipsService,
    private planetService: PlanetsService,
    private speciesService: SpeciesService,
    private vehiclesService: VehiclesService,
  ) { }

  ngOnInit(): void {
    this.peoplesService.getPeoplesList().then(res => {
      this.peoplesList = res['results'];
      this.peopleListObs = this.peoplesService.getPeopleSelected().subscribe(res => {
        if(res.hasOwnProperty('name')) {
          let indexToSplit = res['url'].split('');
          this.urlIndex = indexToSplit[28] + indexToSplit[29];
          this.peoplesList = [];
          this.peopleSelected(res);
          this.nextList();
        }
      })
    })


  }

  peopleSelected(people) {
    this.personeName = people['name'];
    this.people_selected = people;
    this.getMoviesList();
    this.getStarshipsList();
    this.getHomworld();
    this.getSpeciesList();
    this.getVehiclesList();
  }

  getMoviesList() {
    this.peopleMovies = []
    if( this.people_selected.films.length > 0) {
      this.people_selected.films.forEach(element => {
        this.filmsService.getMoviesById(element).subscribe(element => {
         this.peopleMovies.push(element)
       })
     })
    }
  }

  getStarshipsList() {
    this.peopleStarships = [];
    if (this.people_selected.starships.length > 0) {
      this.people_selected.starships.forEach(element => {
        this.starshipsService.getStarshipById(element).subscribe(element => {
          this.peopleStarships.push(element);
        })
      });
    }
  }

  getHomworld() {
    this.homeworld = []
    this.planetService.getPlanetById(this.people_selected['homeworld']).subscribe(element => {
      this.homeworld = element;
    })
  }

  getSpeciesList() {
    this.peopleSpecies = [];
    if (this.people_selected.species.length > 0) {
      this.people_selected.species.forEach(element => {
        this.speciesService.getSpeciesById(element).subscribe(element => {
          this.peopleSpecies.push(element);
        });
      })
    }
  }

  getVehiclesList() {
    this.pilotsVehicles = [];
    if (this.people_selected.vehicles.length > 0) {
      this.people_selected.vehicles.forEach(element => {
        this.vehiclesService.getVehicleById(element).subscribe(element => {
          this.pilotsVehicles.push(element);
        });
      })
    }
  }

  nextList() {
    if (this.urlIndex) {
      this.counter = parseInt(this.urlIndex);
    }
    let newCounter = this.counter + 10;
    if (this.counter <= 82) {
      this.peoplesList = [];

      for (this.counter; this.counter < newCounter && this.counter<=82; this.counter++) {
        if(this.counter === 82) {
          this.finishedList = false
        }
        this.peoplesService.getPeoplesById('http://swapi.dev/api/people/' + this.counter).subscribe(element => {
          if(element.hasOwnProperty('name')) {
            this.peoplesList.push(element)
          }
        })
      }
      this.urlIndex = null;
    }
  }

  redirectToMovieSelected(movie) {
    this.filmsService.setMovieSelected(movie);
  }

  redirectToStarshipSelected(starship) {
    this.starshipsService.setStarshipSelected(starship);
  }

  redirectToPlanetSelected() {
    this.planetService.setPlanetSelected(this.homeworld)
  }

  redirectToSpeciesSelected(species) {
    this.speciesService.setSpeciesSelected(species)
  }

  redirectToVehicleSelected(vehicle) {
    this.vehiclesService.setVehicleSelected(vehicle)
  }

  ngOnDestroy() {
    this.peopleListObs.unsubscribe()
  }
}
