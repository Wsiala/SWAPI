import { Component, OnInit } from '@angular/core';
import { throwError } from 'rxjs';
import { FilmsService } from 'src/app/Services/films.service';
import { PeoplesService } from 'src/app/Services/peoples.service';
import { StarshipsService } from 'src/app/Services/starships.service';

@Component({
  selector: 'app-starships',
  templateUrl: './starships.component.html',
  styleUrls: ['./starships.component.css']
})
export class StarshipsComponent implements OnInit {

  starshipsList;
  starship_selected;
  starshipName;
  starshipMovies;
  counter = 10;
  starshipPilotes;
  urlIndex;
  finishedList = true;

  constructor(
    private starshipsService: StarshipsService,
    private filmsService: FilmsService,
    private peoplesService: PeoplesService,
  ) { }

  ngOnInit(): void {
    this.starshipsService.getStarshipsList().then(res => {
      this.starshipsList = res['results'];
      this.starshipsService.getStarshipSelected().subscribe(res => {
        if(res.hasOwnProperty('name')) {
          let indexToSplit = res['url'].split('');
          this.urlIndex = indexToSplit[31] + indexToSplit[32];
          this.starshipsList = [];
          this.starshipSelected(res);
          this.nextList();
        }
      })
    })


  }

  starshipSelected(starship) {
    this.starshipName = starship['name'];
    this.starship_selected = starship;
    this.getMoviesList();
    this.getPilotesList();
  }

  getMoviesList() {
    this.starshipMovies = []
    if( this.starship_selected.films.length > 0) {
      this.starship_selected.films.forEach(element => {
        this.filmsService.getMoviesById(element).subscribe(element => {
         this.starshipMovies.push(element)
       })
     })
    }
  }

  getPilotesList() {
    this.starshipPilotes = [];
    if( this.starship_selected.pilots.length > 0) {
      this.starship_selected.pilots.forEach(element => {
        this.peoplesService.getPeoplesById(element).subscribe(element => {
         this.starshipPilotes.push(element)
       })
     })
    }
  }

  nextList() {
    if (this.urlIndex) {
      this.counter = parseInt(this.urlIndex);
    }

    let newCounter = this.counter + 10;
    if (this.counter <= 36) {
      this.starshipsList = [];
      for (this.counter; this.counter < newCounter && this.counter<=36; this.counter++) {
        if(this.counter === 36) {
          this.finishedList = false
        }
        this.starshipsService.getStarshipById('http://swapi.dev/api/starships/' + this.counter).subscribe(
          (element) => {
          if(element.hasOwnProperty('name')) {
            this.starshipsList.push(element)
          }
        }),
        (err) =>  {
          throw(err)
        }
      }
      this.urlIndex = null;
    }
  }

  redirectToMovieSelected(movie) {
    this.filmsService.setMovieSelected(movie);
  }

  redirectToPeopleSelected(people) {
    this.peoplesService.setPeopleSelected(people);
  }

}
