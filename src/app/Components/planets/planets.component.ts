import { Component, OnInit } from '@angular/core';
import { FilmsService } from 'src/app/Services/films.service';
import { PeoplesService } from 'src/app/Services/peoples.service';
import { PlanetsService } from 'src/app/Services/planets.service';

@Component({
  selector: 'app-planets',
  templateUrl: './planets.component.html',
  styleUrls: ['./planets.component.css']
})
export class PlanetsComponent implements OnInit {

  planetsList;
  planetName;
  planet_selected;
  counter = 10;
  planetMovies;
  planetResidents;
  urlIndex;
  finishedList = true;

  constructor(
    private planetsService: PlanetsService,
    private filmsService: FilmsService,
    private peoplesService: PeoplesService,
  ) { }

  ngOnInit(): void {
    this.planetsService.getPlanetsList().then(res => {
      this.planetsList = res['results'];
      this.planetsService.getPlanetSelected().subscribe(res => {
        if(res.hasOwnProperty('name')) {
          let indexToSplit = res['url'].split('');
          this.urlIndex = indexToSplit[29] + indexToSplit[30];
          this.planetsList = [];
          this.planetSelected(res);
          this.nextList();
        }
      })
    })

    this.planetsService.getPlanetSelected().subscribe(res => {
      if(res.hasOwnProperty('name')) {
        let indexToSplit = res['url'].split('');
        this.urlIndex = indexToSplit[29] + indexToSplit[30];
        this.planetsList = [];
        this.planetSelected(res);
        this.nextList();
      }
    })
  }

  planetSelected(planet) {
    this.planetName = planet['name'];
    this.planet_selected = planet;
    this.getMoviesList();
    this.getResidentsList();
  }

  nextList() {
    if (this.urlIndex) {
      this.counter = parseInt(this.urlIndex);
    }

    let newCounter = this.counter + 10;
    if (this.counter <= 60) {
      this.planetsList = [];
      for (this.counter; this.counter < newCounter && this.counter<=60; this.counter++) {
        if(this.counter === 60) {
          this.finishedList = false
        }
        this.planetsService.getPlanetById('http://swapi.dev/api/planets/' + this.counter).subscribe(element => {
          if(element.hasOwnProperty('name')) {
            this.planetsList.push(element)
          }
        })
      }
      this.urlIndex = null;
    }
  }

  getMoviesList() {
    this.planetMovies = []
    if( this.planet_selected.films.length > 0) {
      this.planet_selected.films.forEach(element => {
        this.filmsService.getMoviesById(element).subscribe(element => {
         this.planetMovies.push(element)
       })
     })
    }
  }

  getResidentsList() {
    this.planetResidents = [];
    if( this.planet_selected.residents.length > 0) {
      this.planet_selected.residents.forEach(element => {
        this.peoplesService.getPeoplesById(element).subscribe(element => {
         this.planetResidents.push(element)
       })
     })
    }
  }

  redirectToMovieSelected(movie) {
    this.filmsService.setMovieSelected(movie);
  }

  redirectToResidentSelected(resident) {
    this.peoplesService.setPeopleSelected(resident);
  }

}
