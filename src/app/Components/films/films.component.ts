import { Component, OnInit } from '@angular/core';
import { FilmsService } from 'src/app/Services/films.service';
import { PeoplesService } from 'src/app/Services/peoples.service';
import { PlanetsService } from 'src/app/Services/planets.service';
import { SpeciesService } from 'src/app/Services/species.service';
import { StarshipsService } from 'src/app/Services/starships.service';
import { VehiclesService } from 'src/app/Services/vehicles.service';

@Component({
  selector: 'app-films',
  templateUrl: './films.component.html',
  styleUrls: ['./films.component.css']
})
export class FilmsComponent implements OnInit {

  moviesList;
  movie_selected;
  peoplesList;
  moviesPeoples: Array <object>;
  filmTitle;
  moviesStarships;
  moviesPlanets;
  moviesSpecies;
  moviesVehicles;

  constructor(
    private filmsService: FilmsService,
    private peoplesService: PeoplesService,
    private starshipsService: StarshipsService,
    private planetService: PlanetsService,
    private speciesService: SpeciesService,
    private vehiclesService: VehiclesService,
  ) { }

  ngOnInit(): void {
    this.filmsService.getFilmsList().then(res => {
      this.moviesList = res['results'];
    })
    this.filmsService.getMovieSelected().subscribe(movie => {
      if(movie && movie.hasOwnProperty('title')) {
        this.movieSelected(movie);
      }
    })
  }

  movieSelected(movie) {
    this.filmTitle = movie['title'];
    this.movie_selected = movie;
    this.getPeoplesList();
    this.getStarshipsList();
    this.getPlanetsList();
    this.getSpeciesList();
    this.getVehiclesList();
  }

  getPeoplesList() {
    this.moviesPeoples = []
    this.movie_selected.characters.forEach(element => {
       this.peoplesService.getPeoplesById(element).subscribe(element => {
        this.moviesPeoples.push(element)
      })
    })
  }

  getStarshipsList() {
    this.moviesStarships = [];
    if (this.movie_selected.starships.length > 0) {
      this.movie_selected.starships.forEach(element => {
        this.starshipsService.getStarshipById(element).subscribe(element => {
          this.moviesStarships.push(element);
        })
      });
    }
  }

  getPlanetsList() {
    this.moviesPlanets = [];
    if (this.movie_selected.planets.length > 0) {
      this.movie_selected.planets.forEach(element => {
        this.planetService.getPlanetById(element).subscribe(element => {
          this.moviesPlanets.push(element);
        });
      })
    }
  }

  getSpeciesList() {
    this.moviesSpecies = [];
    if (this.movie_selected.species.length > 0) {
      this.movie_selected.species.forEach(element => {
        this.speciesService.getSpeciesById(element).subscribe(element => {
          this.moviesSpecies.push(element);
        });
      })
    }
  }

  getVehiclesList() {
    this.moviesVehicles = [];
    if (this.movie_selected.vehicles.length > 0) {
      this.movie_selected.vehicles.forEach(element => {
        this.vehiclesService.getVehicleById(element).subscribe(element => {
          this.moviesVehicles.push(element);
        });
      })
    }
  }

  redirectToPeopleSelected(people) {
    this.peoplesService.setPeopleSelected(people);
  }

  redirectToStarshipSelected(starship) {
    this.starshipsService.setStarshipSelected(starship);
  }

  redirectToPlanetSelected(planet) {
    this.planetService.setPlanetSelected(planet)
  }

  redirectToSpeciesSelected(species) {
    this.speciesService.setSpeciesSelected(species)
  }

  redirectToVehicleSelected(vehicle) {
    this.vehiclesService.setVehicleSelected(vehicle)
  }
}
