import { Component, OnInit } from '@angular/core';
import { FilmsService } from 'src/app/Services/films.service';
import { PeoplesService } from 'src/app/Services/peoples.service';
import { PlanetsService } from 'src/app/Services/planets.service';
import { SpeciesService } from 'src/app/Services/species.service';

@Component({
  selector: 'app-species',
  templateUrl: './species.component.html',
  styleUrls: ['./species.component.css']
})
export class SpeciesComponent implements OnInit {

  speciesList;
  speciesName;
  species_selected;
  counter = 10;
  speciesMovies;
  speciesResidents;
  speciesPlanet;
  urlIndex;
  finishedList = true;

  constructor(
    private speciesService: SpeciesService,
    private planetsService: PlanetsService,
    private filmsService: FilmsService,
    private peoplesService: PeoplesService,
  ) { }

  ngOnInit(): void {
    this.speciesService.getSpeciesList().then(res => {
      this.speciesList = res['results'];
      this.speciesService.getSpeciesSelected().subscribe(res => {
        if(res.hasOwnProperty('name')) {
          let indexToSplit = res['url'].split('');
          this.urlIndex = indexToSplit[29] + indexToSplit[30];
          this.speciesList = [];
          this.speciesSelected(res);
          this.nextList();
        }
      })
    })

    this.speciesService.getSpeciesSelected().subscribe(res => {
      if(res.hasOwnProperty('name')) {
        let indexToSplit = res['url'].split('');
        this.urlIndex = indexToSplit[29] + indexToSplit[30];
        this.speciesList = [];
        this.speciesSelected(res);
        this.nextList();
      }
    })
  }

  speciesSelected(species) {
    this.speciesName = species['name'];
    this.species_selected = species;
    this.getMoviesList();
    this.getResidentsList();
  }

  nextList() {
    if (this.urlIndex) {
      this.counter = parseInt(this.urlIndex);
    }

    let newCounter = this.counter + 10;
    if (this.counter <= 37) {
      this.speciesList = [];
      for (this.counter; this.counter < newCounter && this.counter<=37; this.counter++) {
        if(this.counter === 37) {
          this.finishedList = false
        }
        this.planetsService.getPlanetById('http://swapi.dev/api/species/' + this.counter).subscribe(element => {
          if(element.hasOwnProperty('name')) {
            this.speciesList.push(element)
          }
        })
      }
      this.urlIndex = null;
    }
  }

  getMoviesList() {
    this.speciesMovies = []
    if( this.species_selected.films.length > 0) {
      this.species_selected.films.forEach(element => {
        this.filmsService.getMoviesById(element).subscribe(element => {
         this.speciesMovies.push(element)
       })
     })
    }
  }

  getResidentsList() {
    this.speciesResidents = [];
    if( this.species_selected.people.length > 0) {
      this.species_selected.people.forEach(element => {
        this.peoplesService.getPeoplesById(element).subscribe(element => {
         this.speciesResidents.push(element)
       })
     })
    }
  }

  redirectToMovieSelected(movie) {
    this.filmsService.setMovieSelected(movie);
  }

  redirectToResidentSelected(resident) {
    this.peoplesService.setPeopleSelected(resident);
  }

}
