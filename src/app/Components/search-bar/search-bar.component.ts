import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FilmsService } from 'src/app/Services/films.service';
import { PeoplesService } from 'src/app/Services/peoples.service';
import { PlanetsService } from 'src/app/Services/planets.service';
import { SpeciesService } from 'src/app/Services/species.service';
import { StarshipsService } from 'src/app/Services/starships.service';
import { VehiclesService } from 'src/app/Services/vehicles.service';

@Component({
  selector: 'app-search-bar',
  templateUrl: './search-bar.component.html',
  styleUrls: ['./search-bar.component.css']
})
export class SearchBarComponent implements OnInit {

  textFilter;
  searchList;
  timeout;
  count= {
    films: 0,
    person: 0,
    starships: 0,
    planets: 0,
    species: 0,
    vehicles: 0,
  };
  nameSearchActivated = false;
  titleSearchActivated = false;
  modelSearchActivated = false;

  constructor(
    private peoplesService: PeoplesService,
    private filmsService: FilmsService,
    private starshipsService: StarshipsService,
    private planetService: PlanetsService,
    private speciesService: SpeciesService,
    private vehiclesService: VehiclesService,
    private router: Router
    ) { }

  ngOnInit(): void {
    this.peoplesService.getPeoplesList().then(res => {
      this.count.person = res['count']
    })
    this.filmsService.getFilmsList().then(res => {
      this.count.films = res['count'];
    })
    this.starshipsService.getStarshipsList().then(res => {
      this.count.starships = res['count'];
    })
    this.planetService.getPlanetsList().then(res => {
      this.count.planets = res['count'];
    })
    this.speciesService.getSpeciesList().then(res => {
      this.count.species = res['count'];
    })
    this.vehiclesService.getVehiclesList().then(res => {
      this.count.vehicles = res['count'];
    })
  }

  textFilterChanged() {
    this.searchList = [];
    let searchFilter;
    clearTimeout(this.timeout);
    this.timeout = setTimeout(() => {
      searchFilter = this.textFilter
      .replace(/[\-\[\]\/\{\}\(\)\*\+\?\.\\\^\$\|]/g, '\\$&')
      .replace(/œ|oe/i, '[œ|oe]')
      .replace(/ae|æ/i, '[ae|æ]')
      .replace(/[éèëeê]/i, '[éèëeê]')
      .replace(/[aâäà]/i, '[aâäà]')
      .replace(/[cç]/i, '[cç]')
      .replace(/[iïî]/i, '[iïî]')
      .replace(/[uûù]/i, '[uûù]')
      .replace(/[oóô]/i, '[oóô]')
      .replace(/[ÿy]/i, '[ÿy]')
      .toUpperCase();
      this.triggerNewSearch(searchFilter);
    }, 777);
  }

  triggerNewSearch(filter) {
    this.nameSearchActivated = false;
    this.titleSearchActivated = false;
    this.modelSearchActivated = false;

    for (let index = 0; index <= this.count.person; index++) {
      this.peoplesService.getPeoplesById('http://swapi.dev/api/people/' + index).subscribe(element => {
        if(element.hasOwnProperty('name')) {
          let result;
          let personName = element['name'].toUpperCase();
          result = personName.match(filter);
          if (result) {
            this.nameSearchActivated = true;
            element['category'] = 'people';
            this.searchList.push(element)
          }
        }
      })
    }

    for (let index = 0; index <= this.count.films; index++) {
      this.filmsService.getMoviesById('http://swapi.dev/api/films/' + index).subscribe(element => {
        if (element.hasOwnProperty('title')) {
          let result;
          let movieName = element['title'].toUpperCase();
          result = movieName.match(filter);
          if (result) {
            this.titleSearchActivated = true;
            element['category'] = 'films';
            this.searchList.push(element)
          }
        }
      })
    }

    for (let index = 0; index <= this.count.starships; index++) {
      this.starshipsService.getStarshipById('http://swapi.dev/api/starships/' + index).subscribe(element => {
       if (element.hasOwnProperty('name')) {
        let resultName;
        let ResultModel;
        let starshipName = element['name'].toUpperCase();
        let starshipModel = element['model'].toUpperCase();
        resultName = starshipName.match(filter);
        if (resultName) {
          this.nameSearchActivated = true;
          element['category'] = 'starships'
          this.searchList.push(element)
        }
        ResultModel = starshipModel.match(filter)
        if (ResultModel) {
          this.modelSearchActivated = true;
          element['category'] = 'starships'
          this.searchList.push(element)
        }
       }
      })
    }

    for (let index = 0; index <= this.count.planets; index++) {
      this.planetService.getPlanetById('http://swapi.dev/api/planets/' + index).subscribe(element => {
        if (element.hasOwnProperty('name')) {
          let result;
          let planetName = element['name'].toUpperCase();
          result = planetName.match(filter);
          if (result) {
            this.nameSearchActivated = true;
            element['category'] = 'planets'
            this.searchList.push(element)
          }
        }
      })
    }

    for (let index = 0; index <= this.count.species; index++) {
      this.speciesService.getSpeciesById('http://swapi.dev/api/species/' + index).subscribe(element => {
        if (element.hasOwnProperty('name')) {
          let result;
          let speciesName = element['name'].toUpperCase();
          result = speciesName.match(filter);
          if (result) {
            this.nameSearchActivated = true;
            element['category'] = 'species';
            this.searchList.push(element)
          }
        }
      })
    }

    for (let index = 0; index <= this.count.vehicles; index++) {
      this.vehiclesService.getVehicleById('http://swapi.dev/api/vehicles/' + index).subscribe(element => {
        if (element.hasOwnProperty('name')) {
          let resultName;
          let resultModel;
          let vehicleName = element['name'].toUpperCase();
          let vehicleModel = element['model'].toUpperCase();
          resultName = vehicleName.match(filter);
          if (resultName) {
            this.nameSearchActivated = true;
            element['category'] = 'vehicles';
            this.searchList.push(element)
          }
          resultModel = vehicleModel.match(filter);
          if (resultModel) {
            this.modelSearchActivated = true;
            element['category'] = 'vehicles'
            this.searchList.push(element)
          }
        }
      })
    }
  }

  redirectTo(data, category) {
    switch (category) {
      case 'people':
        this.peoplesService.setPeopleSelected(data);
        this.router.navigate(['/people'])
        break

      case 'films':
        this.filmsService.setMovieSelected(data);
        this.router.navigate(['/films']);
        break

      case 'starships':
        this.starshipsService.setStarshipSelected(data);
        this.router.navigate(['/starships']);
        break

      case 'planets':
      this.planetService.setPlanetSelected(data);
      this.router.navigate(['/planets']);
      break

      case 'species':
      this.speciesService.setSpeciesSelected(data);
      this.router.navigate(['/species']);
      break

      case 'vehicles':
        this.vehiclesService.setVehicleSelected(data);
      this.router.navigate(['/vehicles']);
      break
    }
  }


}
