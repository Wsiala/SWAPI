import { Component, OnInit } from '@angular/core';
import { FilmsService } from 'src/app/Services/films.service';
import { PeoplesService } from 'src/app/Services/peoples.service';
import { PlanetsService } from 'src/app/Services/planets.service';
import { SpeciesService } from 'src/app/Services/species.service';
import { VehiclesService } from 'src/app/Services/vehicles.service';

@Component({
  selector: 'app-vehicules',
  templateUrl: './vehicules.component.html',
  styleUrls: ['./vehicules.component.css']
})
export class VehiculesComponent implements OnInit {

  vehiclesList;
  vehicleName;
  vehicle_selected;
  counter = 10;
  vehiclesMovies;
  vehiclesPilots;
  urlIndex;
  finishedList = true;

  constructor(
    private vehiclesService: VehiclesService,
    private speciesService: SpeciesService,
    private planetsService: PlanetsService,
    private filmsService: FilmsService,
    private peoplesService: PeoplesService,
  ) { }

  ngOnInit(): void {
    this.vehiclesService.getVehiclesList().then(res => {
      this.vehiclesList = res['results'];
      this.vehiclesService.getVehicleSelected().subscribe(res => {
        let indexToSplit = res['url'].split('');
        this.urlIndex = indexToSplit[30] + indexToSplit[31];
        if(res.hasOwnProperty('name')) {
          this.vehiclesList = [];
          this.vehicleSelected(res);
          this.nextList();
        }
      })
    })

    this.vehiclesService.getVehicleSelected().subscribe(res => {
      let indexToSplit = res['url'].split('');
      this.urlIndex = indexToSplit[29] + indexToSplit[30];
      if(res.hasOwnProperty('name')) {
        this.vehiclesList = [];
        this.vehicleSelected(res);
        this.nextList();
      }
    })
  }

  vehicleSelected(vehicle) {
    this.vehicleName = vehicle['name'];
    this.vehicle_selected = vehicle;
    this.getMoviesList();
    this.getPilotsList();
  }

  nextList() {
    if (this.urlIndex) {
      this.counter = parseInt(this.urlIndex);
    }

    let newCounter = this.counter + 10;
    if (this.counter <= 39) {
      this.vehiclesList = [];
      for (this.counter; this.counter < newCounter && this.counter<=39; this.counter++) {
        if(this.counter === 39) {
          this.finishedList = false
        }this.vehiclesService.getVehicleById('http://swapi.dev/api/vehicles/' + this.counter).subscribe(element => {
          if(element.hasOwnProperty('name')) {
            this.vehiclesList.push(element)
          }
        })
      }
      this.urlIndex = null;
    }
  }

  getMoviesList() {
    this.vehiclesMovies = []
    if( this.vehicle_selected.films.length > 0) {
      this.vehicle_selected.films.forEach(element => {
        this.filmsService.getMoviesById(element).subscribe(element => {
         this.vehiclesMovies.push(element)
       })
     })
    }
  }

  getPilotsList() {
    this.vehiclesPilots = [];
    if( this.vehicle_selected.pilots.length > 0) {
      this.vehicle_selected.pilots.forEach(element => {
        this.peoplesService.getPeoplesById(element).subscribe(element => {
         this.vehiclesPilots.push(element)
       })
     })
    }
  }

  redirectToMovieSelected(movie) {
    this.filmsService.setMovieSelected(movie);
  }

  redirectToResidentSelected(pilot) {
    this.peoplesService.setPeopleSelected(pilot);
  }

}
